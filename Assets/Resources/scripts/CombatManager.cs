﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CombatManager : MonoBehaviour {

    public List<Combatant> party;
    public List<Combatant> enemies;
    Slots slots;

    // Use this for initialization
    void Start() {
        slots = transform.GetComponentInChildren<Slots>();
        foreach (Combatant combatant in party) {
            slots.occupySlot(combatant, true);
        }


        foreach (Combatant combatant in enemies) {
            slots.occupySlot(combatant, false);
        }

        foreach (Combatant com in party) {
            com.combatStart();
        }
        foreach (Combatant com in enemies) {
            com.combatStart();
        }
    }

    void Update() {
        foreach (Combatant com in party) {
            com.combatUpdate();
        }
        foreach (Combatant com in enemies) {
            com.combatUpdate();
        }
    }
}
