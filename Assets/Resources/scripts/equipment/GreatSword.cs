﻿using UnityEngine;
using System.Collections;

public class GreatSword : Weapon {
    
    public GreatSword(string name, int damageMin, int damageMax) {
        this.Name = name;
        this.DamageMin = damageMin;
        this.DamageMax = damageMax;
        this.WeaponType = Enums.WeaponTypes.BROAD_SWORD;
        this.StatMod =  Enums.StatTypes.STR;
        this.DamageType = Enums.DamageTypes.PHYSICAL;
        this.WeaponRange = Enums.WeaponRanges.MEELE;
        this.Mods.Add(new Mod(Enums.StatTypes.AS, Enums.EventTypes.PERMANENT, 0, "Great swords are slow.", -4));
    }
}