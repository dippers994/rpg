﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager: MonoBehaviour {

    List<Combatant> party = new List<Combatant>();
    List<Combatant> enemies = new List<Combatant>();
    public GameObject combatManager;
    CombatManager combatManagerScript;

    // Use this for initialization
    void Start() {
        party.Add(CharacterGenerator.generateNewCharacter(1, Enums.CharacterClasses.WARRIOR));
        party.Add(CharacterGenerator.generateNewCharacter(1, Enums.CharacterClasses.WARRIOR));
        party.Add(CharacterGenerator.generateNewCharacter(1, Enums.CharacterClasses.WARRIOR));
        party.Add(CharacterGenerator.generateNewCharacter(1, Enums.CharacterClasses.WARRIOR));

        enemies.Add(CharacterGenerator.generateNewCharacter(1, Enums.CharacterClasses.WARRIOR));
        enemies.Add(CharacterGenerator.generateNewCharacter(1, Enums.CharacterClasses.WARRIOR));
        enemies.Add(CharacterGenerator.generateNewCharacter(1, Enums.CharacterClasses.WARRIOR));
        enemies.Add(CharacterGenerator.generateNewCharacter(1, Enums.CharacterClasses.WARRIOR));

        combatManagerScript = Instantiate(combatManager, transform.position, transform.rotation).GetComponent<CombatManager>();
        combatManagerScript.party = party;
        combatManagerScript.enemies = enemies;
    }

    // Update is called once per frame
    void Update() {

    }

    public List<Combatant> Party {
        get {
            return party;
        }

        set {
            party = value;
        }
    }

    public List<Combatant> Enemies {
        get {
            return enemies;
        }

        set {
            enemies = value;
        }
    }
}
