﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ability : IEventTracking{

    string abilityName;
    Combatant caster;
    Enums.TargetingTypes targetingType;
    string desc;
    int cooldown;
    int currentCooldown;
    Enums.AbilityTypes abilityType;
    Enums.EventTypes cooldownLengthType;
    List<Combatant> targets;
    List<Mod> mods;

    public Ability(Combatant caster) {
        this.caster = caster;
    }

    public string AbilityName {
        get {
            return abilityName;
        }

        set {
            abilityName = value;
        }
    }

    public Enums.TargetingTypes TargetingType {
        get {
            return targetingType;
        }

        set {
            targetingType = value;
        }
    }

    public string Desc {
        get {
            return desc;
        }

        set {
            desc = value;
        }
    }

    public int Cooldown {
        get {
            return cooldown;
        }

        set {
            cooldown = value;
        }
    }

    public Enums.EventTypes CooldownLengthType {
        get {
            return cooldownLengthType;
        }

        set {
            cooldownLengthType = value;
        }
    }

    public List<Combatant> Targets {
        get {
            return targets;
        }

        set {
            targets = value;
        }
    }

    public List<Mod> Mods {
        get {
            return mods;
        }

        set {
            mods = value;
        }
    }

    public int CurrentCooldown {
        get {
            return currentCooldown;
        }

        set {
            currentCooldown = value;
        }
    }

    public Enums.AbilityTypes AbilityType {
        get {
            return abilityType;
        }

        set {
            abilityType = value;
        }
    }

    public Combatant Caster {
        get {
            return caster;
        }

        set {
            caster = value;
        }
    }

    public abstract void eventOccurance(Enums.EventTypes eventType);

    public abstract void useAblitity(List<Combatant> targets);
}
