﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatsDisplayPanel : MonoBehaviour{

    Text strText;
    Text dexText;
    Text intText;
    Text defText;
    Text agiText;
    Text resText;
    Text asText;
    Text hitText;
    Text dodgeText;

    Stats statsToDisplay;

    void Start() {
        strText = transform.Find("STRText").GetComponent<Text>();
        dexText = transform.Find("DEXText").GetComponent<Text>();
        intText = transform.Find("INTText").GetComponent<Text>();
        defText = transform.Find("DEFText").GetComponent<Text>();
        agiText = transform.Find("AGIText").GetComponent<Text>();
        resText = transform.Find("RESText").GetComponent<Text>();
        asText = transform.Find("ASText").GetComponent<Text>();
        hitText = transform.Find("HITText").GetComponent<Text>();
        dodgeText = transform.Find("DODGEText").GetComponent<Text>();
    }

    public void setDisplay(Stats statsToDisplay) {
        this.statsToDisplay = statsToDisplay;
    }

    void Update() {
        if (statsToDisplay != null) { 
            strText.text = statsToDisplay.getModStat(Enums.StatTypes.STR) + "";
            dexText.text = statsToDisplay.getModStat(Enums.StatTypes.DEX) + "";
            intText.text = statsToDisplay.getModStat(Enums.StatTypes.INT) + "";
            defText.text = statsToDisplay.getModStat(Enums.StatTypes.DEF) + "";
            agiText.text = statsToDisplay.getModStat(Enums.StatTypes.AGI) + "";
            resText.text = statsToDisplay.getModStat(Enums.StatTypes.RES) + "";
            asText.text = statsToDisplay.getModStat(Enums.StatTypes.AS) + "";
            hitText.text = statsToDisplay.getModStat(Enums.StatTypes.HIT) + "";
            dodgeText.text = statsToDisplay.getModStat(Enums.StatTypes.DODGE) + "";
    }
    }
}
