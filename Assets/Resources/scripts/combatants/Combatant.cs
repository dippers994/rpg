﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Combatant : IEventTracking {

    string combatantName;
    bool male;
    Enums.TargetTypes targetType;
    bool frontRow = true;

    Stats stats = new Stats();
    List<Weapon> weapons = new List<Weapon>();
    public Weapon currentWeapon;

    List<Ability> abilities = new List<Ability>();
    Dictionary<Enums.WeaponTypes, int> proficiencies = new Dictionary<Enums.WeaponTypes, int>();

    private const float BASE_ACTION_TIME = 10f;
    private const float AS_ACTION_SPEED_TIME_REDUCTION = .5f;
    private float currentTimeTillAction;
    public bool canTakeAction = false;
    public bool isDead = false;

    Sprite sprite;
    
    public void combatStart() {
        currentTimeTillAction = calculateActionSpeed();
    }
    public void combatUpdate() {
        if (currentTimeTillAction > 0) {
            canTakeAction = false;
            currentTimeTillAction -= Time.deltaTime;
        } else {
            currentTimeTillAction = 0;
            canTakeAction = true;
        }
    }

    private float calculateActionSpeed() {
        return BASE_ACTION_TIME - (stats.getModStat(Enums.StatTypes.AS) * AS_ACTION_SPEED_TIME_REDUCTION);
    }

    public void attack(List<Combatant> targets) {
        int minDamage = currentWeapon.DamageMin;    
        int maxDamage = currentWeapon.DamageMax;

        int damage = UnityEngine.Random.Range(minDamage, maxDamage) + (stats.getModStat(currentWeapon.StatMod) * 10);

        int hit = stats.getModStat(Enums.StatTypes.HIT);

        if ((frontRow && currentWeapon.WeaponRange.Equals(Enums.WeaponRanges.RANGED))
         || !frontRow && currentWeapon.WeaponRange.Equals(Enums.WeaponRanges.MEELE)) {
            hit -= 5;
        }

        foreach (Combatant target in targets) {
            target.takeDamage(currentWeapon.DamageType, damage, hit);
        }
        this.currentTimeTillAction = calculateActionSpeed();
    }
    public void attack(Combatant target) {
        int minDamage = currentWeapon.DamageMin;
        int maxDamage = currentWeapon.DamageMax;

        int damage = UnityEngine.Random.Range(minDamage, maxDamage) + (stats.getModStat(currentWeapon.StatMod) * 10);

        int hit = stats.getModStat(Enums.StatTypes.HIT);

        if ((frontRow && currentWeapon.WeaponRange.Equals(Enums.WeaponRanges.RANGED))
         || !frontRow && currentWeapon.WeaponRange.Equals(Enums.WeaponRanges.MEELE)) {
            hit -= 5;
        }

        target.takeDamage(currentWeapon.DamageType, damage, hit);

        this.currentTimeTillAction = calculateActionSpeed();
    }

    public void switchRow() {
        if (frontRow) {
            frontRow = false;
        } else {
            frontRow = true;
        }
    }

    public void equipWeapn(Weapon weapon) {
        foreach (Mod mod in weapon.Mods) {
            this.addMod(mod);
        }
        currentWeapon = weapon;
    }

    public void useAbility(Ability abilityToUse, List<Combatant> targets) {
        abilityToUse.useAblitity(targets);
        this.currentTimeTillAction = calculateActionSpeed();
    }

    public void takeDamage(Enums.DamageTypes type, int amount, int hit) {
        bool didHit = true;
        int modHit = (hit - stats.getModStat(Enums.StatTypes.DODGE));

        if (modHit < 0) {
            int missChance = (modHit * -1) * 10;

            int missResult = UnityEngine.Random.Range(1, 100);

            if (missResult < missChance) {
                didHit = false;
                Debug.Log(combatantName + " dodged the attack!");
            }
        }
        if (didHit) {
            int modAmount = amount;
            if (type.Equals(Enums.DamageTypes.PHYSICAL)) {
                int negated = (stats.getModStat(Enums.StatTypes.DEF) * 10);
                Debug.Log(combatantName + " negated " + negated + " damage!");
                 modAmount = (amount - (stats.getModStat(Enums.StatTypes.DEF) * 10));
                if (modAmount < 0) {
                    modAmount = 0;
                }
                
            } else if (type.Equals(Enums.DamageTypes.MAGICAL)) {
                modAmount = (amount - (stats.getModStat(Enums.StatTypes.RES) * 10));

                if (modAmount < 0) {
                    modAmount = 0;
                }
            }

            stats.CurrentHP -= modAmount;
            if (stats.CurrentHP <= 0) {
                isDead = true;
            }
            Debug.Log(combatantName + " takes " + modAmount + " damage!");
        }
    }

    public void heal(int amount) {
        stats.CurrentHP += amount;
        if (stats.CurrentHP > stats.MaxHP) {
            stats.CurrentHP = stats.MaxHP;
        }
    }

    public void addMod(Mod mod) {
        stats.addMod(mod);
    }

    protected void addAbility(Ability abilityToAdd) {
        abilities.Add(abilityToAdd);
    }

    protected void removeAbility(Ability abilityToRemove) {
        abilities.Remove(abilityToRemove);
    }

    public void eventOccurance(Enums.EventTypes eventType) {
        foreach (Ability ability in abilities) {
           ability.eventOccurance(eventType);
        }
        stats.eventOccurance(eventType);
    }

    public string CombatantName {
        get {
            return combatantName;
        }

        set {
            combatantName = value;
        }
    }

    public Enums.TargetTypes TargetType {
        get {
            return targetType;
        }

        set {
            targetType = value;
        }
    }

    public bool FrontRow {
        get {
            return frontRow;
        }

        set {
            frontRow = value;
        }
    }

    public Stats Stats {
        get {
            return stats;
        }

        set {
            stats = value;
        }
    }

    public List<Weapon> Weapons {
        get {
            return weapons;
        }

        set {
            weapons = value;
        }
    }

    public List<Ability> Abilities {
        get {
            return abilities;
        }

        set {
            abilities = value;
        }
    }

    public Dictionary<Enums.WeaponTypes, int> Proficiencies {
        get {
            return proficiencies;
        }

        set {
            proficiencies = value;
        }
    }

    public bool Male {
        get {
            return male;
        }

        set {
            male = value;
        }
    }

    public Sprite Sprite {
        get {
            return sprite;
        }

        set {
            sprite = value;
        }
    }

    public float CurrentTimeTillAction {
        get {
            return currentTimeTillAction;
        }

        set {
            currentTimeTillAction = value;
        }
    }
}
