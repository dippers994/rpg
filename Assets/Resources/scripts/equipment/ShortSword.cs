﻿using UnityEngine;
using System.Collections;

public class ShortSword: Weapon {
    
    public ShortSword(string name, int damageMin, int damageMax) {
        this.Name = name;
        this.DamageMin = damageMin;
        this.DamageMax = damageMax;
        this.WeaponType = Enums.WeaponTypes.SHORT_SWORD;
        this.StatMod =  Enums.StatTypes.STR;
        this.DamageType = Enums.DamageTypes.PHYSICAL;
        this.WeaponRange = Enums.WeaponRanges.MEELE;
        this.Mods.Add(new Mod(Enums.StatTypes.AS, Enums.EventTypes.PERMANENT, 0, "Short swords are fast.", 4));
    }
}