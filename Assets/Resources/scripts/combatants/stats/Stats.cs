﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic
    ;
using System;

public class Stats : IEventTracking{

    int level;
    int maxHP;
    int currentHP;

    Dictionary<Enums.StatTypes, Stat> statMap = new Dictionary<Enums.StatTypes, Stat>();

    public Stats(int level, int maxHP, int STR, int DEX, int INT, int DEF, int AGI, int RES, int AS, int HIT, int DODGE, int CRIT, int CRIT_ATT, int CRIT_DEF) {
        this.level = level;
        this.maxHP = maxHP;
        this.currentHP = this.maxHP;

        statMap.Add(Enums.StatTypes.STR, new Stat(Enums.StatTypes.STR, STR));
        statMap.Add(Enums.StatTypes.DEX, new Stat(Enums.StatTypes.DEX, DEX));
        statMap.Add(Enums.StatTypes.INT, new Stat(Enums.StatTypes.INT, INT));
        statMap.Add(Enums.StatTypes.DEF, new Stat(Enums.StatTypes.DEF, DEF));
        statMap.Add(Enums.StatTypes.AGI, new Stat(Enums.StatTypes.AGI, AGI));
        statMap.Add(Enums.StatTypes.RES, new Stat(Enums.StatTypes.RES, RES));

        statMap.Add(Enums.StatTypes.AS, new Stat(Enums.StatTypes.AS, AS));
        statMap.Add(Enums.StatTypes.HIT, new Stat(Enums.StatTypes.HIT, HIT));
        statMap.Add(Enums.StatTypes.DODGE, new Stat(Enums.StatTypes.DODGE, DODGE));
        statMap.Add(Enums.StatTypes.CRIT, new Stat(Enums.StatTypes.CRIT, CRIT));
        statMap.Add(Enums.StatTypes.CRIT_ATT, new Stat(Enums.StatTypes.CRIT_ATT, CRIT_ATT));
        statMap.Add(Enums.StatTypes.CRIT_DEF, new Stat(Enums.StatTypes.CRIT_DEF, CRIT_DEF));
    }

    public int Level {
        get {
            return level;
        }

        set {
            level = value;
        }
    }

    public Dictionary<Enums.StatTypes, Stat> StatList {
        get {
            return statMap;
        }

        set {
            statMap = value;
        }
    }

    public int MaxHP {
        get {
            return maxHP;
        }

        set {
            maxHP = value;
        }
    }

    public int CurrentHP {
        get {
            return currentHP;
        }

        set {
            currentHP = value;
        }
    }

    public Stats() {
        foreach (Enums.StatTypes statType in System.Enum.GetValues(typeof(Enums.StatTypes))) {
            statMap.Add(statType, new Stat(statType));
        }
    }

    public int getModStat(Enums.StatTypes statType) {
        return statMap[statType].ModValue;
    }

    public void addMod(Mod mod) {
        statMap[mod.StatType].addMod(mod);
    }

    public void eventOccurance(Enums.EventTypes eventType) {
        foreach (KeyValuePair<Enums.StatTypes, Stat> entry in statMap) {
            entry.Value.eventOccurance(eventType);
        }
    }
}
