﻿using UnityEngine;
using System.Collections;

public class Enums {

    public enum StatTypes { STR, DEX, INT, DEF, AGI, RES, AS, HIT, DODGE, CRIT, CRIT_ATT, CRIT_DEF};
    public enum AbilityTypes { ACTIVE, PASSIVE };
    public enum TargetingTypes { All_ENEMIES, All_ALLIES, SINGLE_ENEMY, SINGLE_ALLY, SELF };
    public enum DamageTypes { PHYSICAL, MAGICAL, INSTANT };
    public enum TargetTypes { ALLY, ENEMY };
    public enum EventTypes { ROUND_END, FIGHT_END, REST, ENEMY_SLAIN, LEVEL_UP, PERMANENT };
    public enum Rarities { COMMON, UNCOMMON, RARE, VERY_RARE, LEGENDARY };
    public enum WeaponTypes { LONG_SWORD, SHORT_SWORD, BROAD_SWORD, GREAT_SWORD, RAPIER, LARGE_KNIFE, DAGGER, BOW, STAFF, ORB, ROD, TOME, HAMMER, SCIMITAR, WAND };
    public enum WeaponRanges { MEELE, RANGED, HYBRID };
    public enum CharacterClasses { WARRIOR, RANGER, CASTER, KNIGHT, ASSASSIN, CLERIC };
}
