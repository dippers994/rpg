﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Slots : MonoBehaviour {

    List<Slot> characterSlotList = new List<Slot>();
    List<Slot> enemySlotList = new List<Slot>();
    Slot selectedSlot;
    public Canvas canvas;
    public GameObject selectionPanel;
    public GameObject abilityPanel;
    public GameObject abilityButtonPrefab;

    bool showAbilities = false;
    bool targeting = false;

    public Text nameDisplay;
    public Text weaponDisplay;
    public StatsDisplayPanel statsDisplayPanel;

    // Use this for initialization
    void Awake() {
        foreach (Transform child in transform) {
            if (child.gameObject.GetComponent<Slot>().playerSide) {
                characterSlotList.Add(child.gameObject.GetComponent<Slot>());
                child.gameObject.GetComponent<Slot>().playerSide = true;
            } else {
                enemySlotList.Add(child.gameObject.GetComponent<Slot>());
                child.gameObject.GetComponent<Slot>().playerSide = false;
            }
        }
    }

    // Update is called once per frame
    void Update() {

    }

    public bool occupySlot(Combatant combatant, bool playerSide) {
        int slotNum = 0;
        if (playerSide) {
            while (characterSlotList[slotNum].Occupied) {
                slotNum++;
            }
            characterSlotList[slotNum].fillSlot(combatant);
            return characterSlotList[slotNum].frontRow;

        } else {
            while (enemySlotList[slotNum].Occupied)  {
                slotNum++;
            } 
            enemySlotList[slotNum].fillSlot(combatant);
            return enemySlotList[slotNum].frontRow;
        }
    }

    public void slotSelected(Slot newSlot, bool playerSide) {
        if (playerSide) {
            if (!targeting) {
                nameDisplay.text = newSlot.CombatantInSlot.CombatantName;
                this.selectedSlot = newSlot;
                weaponDisplay.text = selectedSlot.CombatantInSlot.currentWeapon.Name;
                statsDisplayPanel.setDisplay(selectedSlot.CombatantInSlot.Stats);
                canvas.enabled = true;
            } else {

            }
        } else {
            if (selectedSlot.CombatantInSlot.canTakeAction) {
                this.selectedSlot.CombatantInSlot.attack(newSlot.CombatantInSlot);
            }
        }
    }

    public void attackPressed() {

    }

    public void abilitiesPressed() {
        showAbilities = true;

        selectionPanel.SetActive(false);
        abilityPanel.SetActive(true);
        foreach (Ability ability in selectedSlot.CombatantInSlot.Abilities) {
            GameObject button = Instantiate(abilityButtonPrefab);
            if (ability.CurrentCooldown > 0) {
                button.GetComponent<Text>().text = ability.AbilityName + ability.CurrentCooldown + " remaining";
                button.GetComponentInChildren<Button>().interactable = false;
            } else {
                button.GetComponent<Text>().text = ability.AbilityName + " " + ability.Cooldown;
                button.GetComponentInChildren<Button>().onClick.AddListener(
                    () => { useAblityPressed(ability); }
                    );
            }
            button.transform.parent = abilityPanel.transform;
        }

    }

    public void useAblityPressed(Ability ability) {

    }

    public void dodgePressed() {

    }

    public void switchRowsPressed() {
        if (selectedSlot.playerSide) {
            foreach (Slot slot in characterSlotList) {
                if (!slot.Occupied && slot.frontRow == !selectedSlot.frontRow) {
                    slot.fillSlot(selectedSlot.CombatantInSlot);
                    selectedSlot.removeSlot();
                    selectedSlot = slot;
                    break;
                }
            }
        }
    }
}
