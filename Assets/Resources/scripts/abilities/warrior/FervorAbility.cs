﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class FervorAbility : ClassAbility, IEventTracking {

    const float hpPercentHeal = 5;

    public FervorAbility(Combatant caster) : base(caster)  {
        this.AbilityName = "Fervor";
        this.Desc = "Heals for a percentage of max health when an enemy is slain.";
        this.CooldownLengthType = Enums.EventTypes.ENEMY_SLAIN;
        this.Cooldown = 3;
        this.CurrentCooldown = 0;
        this.TargetingType = Enums.TargetingTypes.All_ENEMIES;
    }

    public override void eventOccurance(Enums.EventTypes eventType) {
        if (this.CooldownLengthType.Equals(eventType)) {
            int amount = (int)(hpPercentHeal / this.Caster.Stats.MaxHP) * 100;
            this.Caster.heal(amount);
        }
    }

    public override void useAblitity(List<Combatant> targets) {}
}
