﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SlashingStrikeAbility : ClassAbility, IEventTracking {

    public SlashingStrikeAbility(Combatant caster) : base(caster) {
        this.AbilityName = "Slashing Strike";
        this.Desc = "Does damage to all enemies.";
        this.CooldownLengthType = Enums.EventTypes.ROUND_END;
        this.Cooldown = 3;
        this.CurrentCooldown = 0;
        this.TargetingType = Enums.TargetingTypes.All_ENEMIES;
    }

    public override void eventOccurance(Enums.EventTypes eventType) {
        if (this.CooldownLengthType.Equals(eventType)) {
            if (CurrentCooldown > 0) {
                CurrentCooldown -= 1;
            }
        }
    }

    public override void useAblitity(List<Combatant> targets) {
        if (CurrentCooldown <= 0) {
            this.Caster.attack(targets);
            CurrentCooldown = Cooldown;
        }
        
    }
}
