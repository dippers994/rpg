﻿using UnityEngine;
using System.Collections;
using System;

public abstract class ClassAbility : Ability {

    int unlockLevel;

    public ClassAbility(Combatant caster) : base(caster) {}

    public int UnlockLevel {
        get {
            return unlockLevel;
        }

        set {
            unlockLevel = value;
        }
    }
}
