﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Equipment {

    string name;
    string desc;
    Enums.Rarities rarity;
    int requiredLevel;
    List<Mod> mods = new List<Mod>();
    List<Ability> abilities = new List<Ability>();

    public string Name {
        get {
            return name;
        }

        set {
            name = value;
        }
    }

    public string Desc {
        get {
            return desc;
        }

        set {
            desc = value;
        }
    }

    public List<Mod> Mods {
        get {
            return mods;
        }

        set {
            mods = value;
        }
    }

    public List<Ability> Abilities {
        get {
            return abilities;
        }

        set {
            abilities = value;
        }
    }
}
