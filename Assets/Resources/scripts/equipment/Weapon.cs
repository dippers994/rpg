﻿using UnityEngine;
using System.Collections;

public class Weapon : Equipment {

    int damageMin;
    int damageMax;
    Enums.WeaponTypes weaponType;
    Enums.StatTypes statMod;
    Enums.DamageTypes damageType;
    Enums.WeaponRanges weaponRange;

    public int DamageMin {
        get {
            return damageMin;
        }

        set {
            damageMin = value;
        }
    }

    public int DamageMax {
        get {
            return damageMax;
        }

        set {
            damageMax = value;
        }
    }

    public Enums.StatTypes StatMod {
        get {
            return statMod;
        }

        set {
            statMod = value;
        }
    }

    public Enums.WeaponTypes WeaponType {
        get {
            return weaponType;
        }

        set {
            weaponType = value;
        }
    }

    public Enums.DamageTypes DamageType {
        get {
            return damageType;
        }

        set {
            damageType = value;
        }
    }

    public Enums.WeaponRanges WeaponRange {
        get {
            return weaponRange;
        }

        set {
            weaponRange = value;
        }
    }
}
