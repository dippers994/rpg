﻿using UnityEngine;
using System.Collections;
using System;

public interface IEventTracking {

    void eventOccurance(Enums.EventTypes eventType);
}
