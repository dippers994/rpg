﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Stat : IEventTracking {

    Guid timeTrackingId;
    Enums.StatTypes statType;
    int baseValue;
    int modValue;

    List<Mod> mods = new List<Mod>();

    public Stat(Enums.StatTypes statType, int baseValue) {
        this.statType = statType;
        this.baseValue = baseValue;
        calculateMods();
    }

    public Enums.StatTypes StatType {
        get {
            return statType;
        }

        set {
            statType = value;
        }
    }

    public int BaseValue {
        get {
            return baseValue;
        }

        set {
            baseValue = value;
        }
    }

    public int ModValue {
        get {
            return modValue;
        }

        set {
            modValue = value;
        }
    }

    public Stat(Enums.StatTypes type) {
        StatType = type;
    }

    public void addMod(Mod mod) {
        mods.Add(mod);
        calculateMods();
    }

    public void removeMod(Mod mod) {

        if (mod != null) {
            mods.Remove(mod);
        }

        calculateMods();
    }

    public void calculateMods() {
        modValue = baseValue;
        foreach (Mod mod in mods) {
            modValue += mod.Value;
        }
    }

    public void eventOccurance(Enums.EventTypes eventType) {
        List<Mod> modsToRemove = new List<Mod>();
        foreach (Mod mod in mods) {
            if (mod.EventType.Equals(eventType)) {
                mod.NumEventsTillExpiration -= 1;
                if (mod.NumEventsTillExpiration <= 0) {
                    modsToRemove.Add(mod);
                }
            }
        }

        foreach (Mod mod in modsToRemove) {
            removeMod(mod);
        }
    }
}
