﻿using UnityEngine;
using System.Collections;
using System;

public class WarriorClass : CharacterClass {
    
    public WarriorClass(string characterName, bool male) {
        this.CombatantName = characterName;
        this.Male = male;
        if (male) {
            this.Sprite = Resources.Load<Sprite>("sprites/MaleWarrior");
        } else {
            this.Sprite = Resources.Load<Sprite>("sprites/FemaleWarrior");
        }
        this.ClassName = "Warrior";
        this.setBaseStats(new Stats(5, 150, 4, 2, 2, 3, 3, 3, 0, 0, 0, 0, 0, 0));
        this.addAbility(new SlashingStrikeAbility(this));
        this.addAbility(new FervorAbility(this));

    }

    public WarriorClass(string characterName, bool male, Stats stats) {
        this.CombatantName = characterName;
        this.Male = male;
        if (male) {
            this.Sprite = Resources.Load<Sprite>("sprites/MaleWarrior");
        } else {
            this.Sprite = Resources.Load<Sprite>("sprites/FemaleWarrior");
        }
        this.ClassName = "Warrior";
        this.setBaseStats(stats);
        this.addAbility(new SlashingStrikeAbility(this));
        this.addAbility(new FervorAbility(this));
    }

    private void setProficiencies() {

        foreach (Enums.WeaponTypes type in Enum.GetValues(typeof(Enums.WeaponTypes))) {
            this.Proficiencies.Add(type,0);
        }
    }
}
