﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class Slot : MonoBehaviour {

    SpriteRenderer spriteRenderer;
    Combatant combatantInSlot;
    bool occupied = false;
    public bool frontRow;
    public bool playerSide;
    public bool selected = false;
    Slots parent;

    public bool Occupied {
        get {
            return occupied;
        }

        set {
            occupied = value;
        }
    }

    public Combatant CombatantInSlot {
        get {
            return combatantInSlot;
        }

        set {
            combatantInSlot = value;
        }
    }

    // Use this for initialization
    void Awake() {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
        parent = transform.parent.GetComponent<Slots>();
    }

    void Update() {
        if (this.combatantInSlot != null && this.combatantInSlot.isDead) {
            this.combatantInSlot = null;
            spriteRenderer.sprite = null;
        }
    }

    private void OnMouseDown() {
        if (occupied) {
            parent.slotSelected(this, playerSide);
        }
    }

    public void fillSlot(Combatant combatant) {
        this.combatantInSlot = combatant;
        spriteRenderer.sprite = combatant.Sprite;
        occupied = true;
    }

    public void removeSlot() {
        spriteRenderer.sprite = null;
        this.combatantInSlot = null;
        occupied = false;
    }

    void OnGUI() {
        if (combatantInSlot != null) {
           Vector3 v3 = Camera.main.WorldToScreenPoint(transform.position);

            GUI.Label(new Rect(v3.x -60, Screen.height - v3.y + 20, 100, 20), combatantInSlot.CurrentTimeTillAction.ToString("F1"));
            GUI.Label(new Rect(v3.x - 60, Screen.height - v3.y + 10, 100, 20), combatantInSlot.Stats.CurrentHP.ToString());
        }
    }
}
