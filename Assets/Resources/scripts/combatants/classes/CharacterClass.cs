﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class CharacterClass : Character {

    string className;

    public string ClassName {
        get {
            return className;
        }

        set {
            className = value;
        }
    }

    protected void setBaseStats(Stats stats) {
        this.Stats = stats;
    }
}
