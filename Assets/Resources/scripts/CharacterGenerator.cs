﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterGenerator {

    public static Character generateNewCharacter(int level, Enums.CharacterClasses characterClass) {
        bool male = true;
        int genderNum = Random.Range(1, 3);
        if (genderNum == 2) {
            male = false;
        }
        List<Weapon> weapons = new List<Weapon>();
        weapons.Add(new BroadSword("Broad Sword", 20, 50));
        weapons.Add(new ShortSword("Short Sword", 10, 30));
        weapons.Add(new GreatSword("Great Sword", 30, 60));

       Stats stats = new Stats(Random.Range(150,250), Random.Range(150, 250), Random.Range(1, 6), Random.Range(1, 6), Random.Range(1, 6), Random.Range(1, 6), Random.Range(1, 6), Random.Range(1, 6), Random.Range(1, 6), Random.Range(1, 6), Random.Range(1, 6), Random.Range(1, 6), Random.Range(1, 6), Random.Range(1, 6));
        Character newCom =  new WarriorClass(Random.Range(0, 1000).ToString(), male, stats);
        int weaponNum  = Random.Range(0, weapons.Count);
        newCom.equipWeapn(weapons[weaponNum]);
        return newCom;
    }
}
