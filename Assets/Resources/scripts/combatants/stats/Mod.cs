﻿using UnityEngine;
using System.Collections;

public class Mod {

    Enums.StatTypes statType;
    Enums.EventTypes eventType;
    int numEventsTillExpiration;
    string desc;
    int value;

    public Mod(Enums.StatTypes statType, Enums.EventTypes lengthType, int numEventsTillExpiration, string desc, int value) {
        this.statType = statType;
        this.eventType = lengthType;
        this.numEventsTillExpiration = numEventsTillExpiration;
        this.desc = desc;
        this.value = value;
    }

    public int Value {
        get {
            return value;
        }

        set {
            this.value = value;
        }
    }

    public Enums.EventTypes EventType {
        get {
            return eventType;
        }

        set {
            eventType = value;
        }
    }

    public string Desc {
        get {
            return desc;
        }

        set {
            desc = value;
        }
    }

    public int NumEventsTillExpiration {
        get {
            return numEventsTillExpiration;
        }

        set {
            numEventsTillExpiration = value;
        }
    }

    public Enums.StatTypes StatType {
        get {
            return statType;
        }

        set {
            statType = value;
        }
    }
}
