﻿using UnityEngine;
using System.Collections;

public class BroadSword: Weapon {
    
    public BroadSword(string name, int damageMin, int damageMax) {
        this.Name = name;
        this.DamageMin = damageMin;
        this.DamageMax = damageMax;
        this.WeaponType = Enums.WeaponTypes.BROAD_SWORD;
        this.StatMod =  Enums.StatTypes.STR;
        this.DamageType = Enums.DamageTypes.PHYSICAL;
        this.WeaponRange = Enums.WeaponRanges.MEELE;
    }
}